<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license LGPL-3.0+
 * @copyright Thomas Kuhn 2007-2015
 */


/**
 * Namespace
 */
namespace Efg;

/**
 * Class FormdataComments
 *
 * @copyright  Thomas Kuhn 2007-2015
 * @author     Thomas Kuhn <mail@th-kuhn.de>
 * @package    Efg
 */
class FormdataComments
{

	/**
	 * List a particular record
	 * @param array
	 * @return string
	 */
	public function listComments($arrRow)
	{

		$strRet = '';

		$objParent = \Database::getInstance()->prepare("SELECT `id`, `form`, `alias`  FROM tl_formdata WHERE id=?")
			->execute($arrRow['parent']);

		if ($objParent->numRows)
		{
			$strRet .= ' (' . $objParent->form;

			if (strlen($objParent->alias))
			{
				$strRet .= ' - '.$objParent->alias;
			}
			$strRet .= ')';
		}

		return $strRet;
	}

}
