<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license LGPL-3.0+
 * @copyright Thomas Kuhn 2007-2015
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['formdata'] = 'Form data';
$GLOBALS['TL_LANG']['MOD']['efg'] = 'Form data';
$GLOBALS['TL_LANG']['MOD']['feedback'] = array('All results', 'Stored data from forms.');
$GLOBALS['TL_LANG']['MOD']['formdata_from'] = 'Stored data from form';
$GLOBALS['TL_LANG']['MOD']['formdatalisting'] = array('Listing form data', 'This module allows you to list the records of a certain form data table in the front end.');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['formdatalisting'] = array('Listing form data', 'Use this module to list the records of a certain form data table in the front end.');
